CC=g++

LFLAGS= -lGLEW -lglut -lGLU -lGL
CFLAGS= -Wall -pedantic -std=c++14

HEADERS =  shaders.h
OBJECTS := shaders.o sinewave3D-glm.o
BIN := ass2

#all: FULL
all: quick

quick:
	$(CC) shaders.c sinewave3D-glm.cpp -o $(BIN) $(LFLAGS) $(CFLAGS) 

FULL:
	g++ -std=c++14 shaders.c sinewave3D-glm.cpp -lGLEW -lglut -lGLU -lGL -lm

# Remove objects and binaries
clean:
		rm  $(OBJECTS) $(BIN)
