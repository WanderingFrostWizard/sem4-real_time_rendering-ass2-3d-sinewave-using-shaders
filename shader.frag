
// Written by Cameron Watt and John Nguyen

// shader.frag

// INPUT
varying vec4 osVert;
varying vec4 esVert;
varying vec4 csVert;
varying vec3 normal;
varying vec3 newColor;

void main (void)
{
  gl_FragColor = vec4 ( newColor, 1);
}