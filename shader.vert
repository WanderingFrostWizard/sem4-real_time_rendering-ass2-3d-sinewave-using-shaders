
// Written by Cameron Watt and John Nguyen

//INPUT
uniform mat4 mvMat;
uniform mat3 nMat;
uniform float shininess;

uniform vec3 Ma;
uniform vec3 Md;
uniform vec3 Ms;

uniform vec3 La;
uniform vec3 Ld;
uniform vec3 Ls;

// 1 for TRUE, 0 for FALSE
uniform int phongLighting;
uniform int positionalLighting;

// OUTPUT
varying vec4 esVert;
varying vec4 csVert;
varying vec3 nVert;

varying vec3 newColor;

/* Perform ADS - ambient, diffuse and specular - lighting calculation
 * in eye coordinates (EC).
 */
vec3 computeLighting()
{
  vec3 nEC = nMat * gl_Normal;

  // Used to accumulate ambient, diffuse and specular contributions
  // Note: it is a vec3 being constructed with a single value which
  // is used for all 3 components
  vec3 color = vec3 ( 0.0 );

  // Ambient contribution: A=La×Ma
  // Default light ambient color and default ambient material color
  // are both (0.2, 0.2, 0.2)

  vec3 ambient = vec3 ( La * Ma );
  color += ambient;

  // Light direction vector. Default for LIGHT0 is a directional light
  // along z axis for all vertices, i.e. <0, 0, 1>
  vec3 lEC = vec3 ( 0.0, 0.0, 1.0 );

  if ( positionalLighting == 1 )
    lEC = lEC - vec3(gl_Vertex);

  lEC = normalize(lEC);

  // Test if normal points towards light source, i.e. if polygon
  // faces toward the light - if not then no diffuse or specular
  // contribution
  float dp = dot(nEC, lEC);
  if (dp > 0.0) {
    // Calculate diffuse and specular contribution

    // Lambert diffuse: D=Ld×Md×cosθ
    // Ld: default diffuse light color for GL_LIGHT0 is white (1.0, 1.0, 1.0).
    // Md: default diffuse material color is grey (0.8, 0.8, 0.8).

    // Need normalized normal to calculate cosθ,
    // light vector <0, 0, 1> is already normalized
    nEC = normalize(nEC);
    float NdotL = dot(nEC, lEC);
    vec3 diffuse = vec3 ( Ld * Md * NdotL );
    color += diffuse;

    // Blinn-Phong specular: S=Ls×Ms×cosⁿα
    // Ls: default specular light color for LIGHT0 is white (1.0, 1.0, 1.0)
    // Ms: specular material color, also set to white (1.0, 1.0, 1.0),
    // but default for fixed pipeline is black, which means can't see
    // specular reflection. Need to set it to same value for fixed
    // pipeline lighting otherwise will look different.

    // Default viewer is at infinity along z axis <0, 0, 1> i.e. a
    // non local viewer (see glLightModel and GL_LIGHT_MODEL_LOCAL_VIEWER)
    vec3 vEC = vec3 ( 0.0, 0.0, 1.0 );

    // Forward declaration of the specular vec3
    vec3 specular;

    if ( phongLighting == 1 )
    {
      // Phong Specular Lighting calculations
      vec3 reflectDir = reflect( -lEC, nEC );
      reflectDir = normalize(reflectDir);
      float VdotR = dot( vEC, reflectDir );
      VdotR = max ( VdotR, 0.0 );
      specular = vec3 ( Ls * Ms * pow(VdotR, shininess) );
    }
    else
    {
      // Blinn-Phong half vector (using a single capital letter for
      // variable name!). Need normalized H (and nEC, above) to calculate cosα.
      vec3 H = vec3 ( lEC + vEC );
      H = normalize(H);
      float NdotH = dot(nEC, H);
      if (NdotH < 0.0)
        NdotH = 0.0;
      specular = vec3 ( Ls * Ms * pow(NdotH, shininess) );
    }
    color += specular;
  }
  return color;
}

void main(void)
{
  // os - object space, es - eye space, cs - clip space
  // Objectspace of the current vertex
  vec4 osVert = gl_Vertex;

  // Eyespace of current vertex
  esVert = 4 mvMat * osVert;
  // esVert = gl_ModelViewMatrix * osVert;

  // Clipspace
  csVert = gl_ProjectionMatrix * esVert;

  // Normal
  nVert = nMat * gl_Normal;

  // Set position of the vertex
  gl_Position = csVert;

  newColor = computeLighting();
}
